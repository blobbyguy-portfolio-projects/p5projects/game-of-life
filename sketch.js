//https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life
//https://www.youtube.com/watch?v=FWSR_7kZuYg&t=1306s


let numRows = 150;
let numCols = 150;
let squareSize = 5;
let initialChangeOfLife = 0.2;

var width;
var height;


var gameArray = new Array(numCols);
function setup() {
  width = numCols * squareSize;
  height = numRows * squareSize;
  createCanvas(width, height);
  frameRate(10);
  for(var i = 0; i < numCols; i++){
    gameArray[i] = new Array(numRows);
    for (var j = 0; j < numRows; j++){
      gameArray[i][j] = Math.random() < initialChangeOfLife ? 1 : 0;
    }
  }
}

function draw() {
  background(100);
  drawGame();
  console.log("next round");
  nextRound();
}

function nextRound(){
  let newArray = new Array(numCols);

  for(var i = 0; i < numCols; i++){
    newArray[i] = new Array(numRows);
    for (var j = 0; j < numRows; j++){
      var liveNeighbours = getNeighboursSum(i, j);

      if (!isAlive(gameArray[i][j])){
        //text(liveNeighbours, i * squareSize, j * squareSize + 10);
      }

      var deadNeighbours = 8 - liveNeighbours;

      if (isAlive(gameArray[i][j])){
        if (liveNeighbours < 2){
            //Any live cell with fewer than two live neighbours dies, as if by underpopulation.
          newArray[i][j] = 0;
        }else if (liveNeighbours == 2 || liveNeighbours == 3){
          //Any live cell with two or three live neighbours lives on to the next generation.
          newArray[i][j] = 1;
        }else{
          //Any live cell with more than three live neighbours dies, as if by overpopulation.
          newArray[i][j] = 0;
        }
      }else{
        if (liveNeighbours == 3){
        //Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
          newArray[i][j] = 1;
        }else{
          newArray[i][j] = 0;
        }
      }
    }
  }
  gameArray = newArray;
}

function getNeighboursSum(col, row){
  var sum = 0;
  for (var i = -1; i <= 1; i++){
    for (var j = -1; j <= 1; j++){
      let currentCol = col + i;
      let currentRow = row + j;

      //Early exit if required
      // console.log("currentCol " + currentCol + " numCols" + numCols);
      // console.log("currentRow " + currentCol + " numRows" + numCols);
      if ((currentCol < 0 || currentCol >= numCols
        || currentRow < 0 || currentRow >= numRows ||
         (i == 0 && j == 0))){
          continue;
      }
      //0 = dead, 1 = alive
      sum += gameArray[currentCol][currentRow];
    }
  }

  return sum;
}

//Input 0 or 1
//Return bool
function isAlive(item){
  return item == 1;
}

function drawGame(){
  for(var i = 0; i < numCols; i++){
    for (var j = 0; j < numRows; j++){
      stroke(0);
      if (isAlive(gameArray[i][j])){
        fill(0);
      }
      else{
        fill(255);
      }
      rect(i * squareSize - 1, j * squareSize - 1, squareSize);
    }
  }
}
